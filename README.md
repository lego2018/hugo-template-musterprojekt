# [Leo's Hugo Template Example Project](https://gitlab.com/lego2018/hugo-template-musterprojekt.git)

In this project we work with `[nps](https://www.npmjs.com/package/nps)`. Therefore we have beside the obligatory `package.json` a file called `package-scripts.js`. Due to it's `.js` format we can comment all our tasks which would not be possible in `package.json`.

The tasks in `package-scripts.js` are started with a leading `nps <taskname>`.

In our `package.json` we only have the dependencies (including `nps`) and npm scripts to start the tasks from `package-scripts.js`.

These are the current scripts in `package.json`:

- npm start: ./bash-scripts/start.sh
- npm stop: nps end
- npm run dev: nps build.development
- npm run dev-disk: nps build.development.hugo_disk
- npm run stage: nps build.stage
- npm run local: nps build.local
- npm run prod: nps build.production
- npm run prod-nomin: nps build.productionNomin
- npm run metricsOn: nps helper.metricsOn
- npm run metricsOff: nps helper.metricsOff

## The npm scripts explained

### `npm start`

This script starts `./bash-scripts/start.sh` which installs, cleans up the `node_modules` and copies the needed modules to `./assets/dependencies` from where they'll be imported by SCSS and Hugo later. Finally the script copies the images, favicons and webfonts into `./static`.

### `npm stop`

This script starts `nps end` in `package-scripts.js` which is doing `nps nodereset`, `nps clean` and `nps helper.metricsOff`.

- `nodereset`: This task from `package-script.js` discards `./assets/dependencies`.
- `nps clean`: This task from `package-script.js` empties `./static`, `./build/dev`, `./build/stage`, `./build/local` and `./public`.
- `nps helper.metricsOff`: This task from `package-script.js` discards `./public`.

### `npm run dev`

This script starts `nps build.development` in `package-scripts.js` which is starting Hugo's server for development serving the files from RAM (default).

### `npm run dev-disk`

This script starts `nps build.development.hugo_disk` in `package-scripts.js` which is starting Hugo's server for development serving the files from folder `./public`.

### `npm run stage`

This script starts `nps build.stage` in `package-scripts.js` which is starting `./bash-scripts/stage.sh`. This bash script has a condition that it only builds if you answered the question with yes.

In the positive case it runs `nps clean.stage` and `nps build.stage.hugo`.

- `nps clean.stage`: This task from `package-script.js` empties `./build/stage`.
- `nps build.stage.hugo`: This task from `package-script.js` renders the site to `./build/stage` with the correct `URL` if you customized the relevant files before (see "answered question" above).

### `npm run local`

This script starts `nps build.local` in `package-scripts.js` which is starting `nps clean.local`, `nps build.local.hugo` and `nps clean.htaccesslocal`.

- `nps clean.local`: This task from `package-script.js` empties `./build/local`.
- `nps build.local.hugo`: This task from `package-script.js` renders the site to `./build/local` with the correct `URL`.
- `nps clean.htaccesslocal`: This task from `package-script.js` discards the `.htaccess` from `./build/local`.

### `npm run prod`

This script starts `nps build.production` in `package-scripts.js` which is starting `./bash-scripts/production.sh`. This bash script has a condition that it only builds if you answered the question with yes.

In the positive case it runs `nps clean.public`, `nps build.production.hugo`, `nps copy.htaccess`, `nps build.production.minifyHtml` and `nps build.production.minifyXml`.

- `nps clean.public`: This task from `package-script.js` empties `./build/public`.
- `nps build.production.hugo`: This task from `package-script.js` renders the site to `./build/public` with the correct `URL` if you customized the relevant files before (see "answered question" above).
- `nps copy.htaccess`: This task from `package-script.js` copies `./assets/htaccess-reset` to `./static/.htaccess`.
- `nps build.production.minifyHtml`: This task from `package-script.js` minifies the html files in `./build/public`.
- `nps build.production.minifyXml`: This task from `package-script.js` minifies the xml files in `./build/public`.

### `npm run prod-nomin`

This script starts `nps build.productionNomin` in `package-scripts.js` which is starting `./bash-scripts/production-nomin.sh`. This bash script has a condition that it only builds if you answered the question with yes.

In the positive case it runs `nps clean.public`, `build.productionNomin.hugo` and `nps copy.htaccess`.

- `nps clean.public`: This task from `package-script.js` empties `./build/public`.
- `nps build.productionNomin.hugo`: This task from `package-script.js` renders the site to `./build/public` with the correct `URL` if you customized the relevant files before (see "answered question" above).
- `nps copy.htaccess`: This task from `package-script.js` copies `./assets/htaccess-reset` to `./static/.htaccess`.

### `npm run metricsOn`

This script starts `nps helper.metricsOn` in `package-scripts.js` which is starting `./bash-scripts/template-metrics.sh` which is running Hugo's tests for rendering duration etc.

### `npm run metricsOff`:

This script starts `nps helper.metricsOff` in `package-scripts.js` which discards `./public`.

## How to work with the template

After cloning the repo to your project directory you'll need to run `npm start` (explanation see above). Once the process is ready you can run the development environment with `npm run dev`.

If you ever want to reset your project you can run `npm stop` (explanation see above).

### CSS and JS

#### SCSS

In this project we're working with SCSS. All scss files are in `./assets/scss`. In production mode the CSS will be minified and fingerprinted.

The CSS is generated by Hugo's asset pipeline. The partial `./layouts/partials/css.html` is processing the CSS. Because `./assets/scss/main.scss` is executed as template we can take advantage of variables which we set in `./assets/scss/main.scss`: `$backgroundColor: {{ .Param "backgroundColor" | default "white" }};` is an example. With this technique you can set the desired background color in `./config.toml`. In case no color is set the background color defaults to `white`. This works with all variables you can think of.

#### JS

The JS is generated by Hugo's asset pipeline. The partial `./layouts/partials/js.html` is processing the JavaScript. First it gets `./assets/js/scripts.json` and imports the listed JS files in order. In production mode the JavaScript will be minified and fingerprinted.

### Menu system

The menu is generated by the Front Matter of the content files. As an example see `./content/contact.md`.

### `postcss.config.js`

This file controls the PostCSS plugins like `autoprefixer`. The use of PostCSS is set in `./layouts/partials/css.html` (`toCSS`).

### `Rakefile.rb`

With this file you can inform the search engines about changes of your site. Just run in terminal `rake notify` (but don't forget to customize the URL of your site before).
