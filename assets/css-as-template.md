Hugo 0.43 added template support for resources with `resources.ExecuteAsTemplate`

So you can do something like this:

```
{{ $styles := resources.Get “scss/main.scss” | resources.ExecuteAsTemplate “style.main.scss” . | toCSS | minify | fingerprint }}
<link rel=”stylesheet” href=”{{ $styles.Permalink }}” integrity=”{{ $styles.Data.Integrity }}” media=”screen”>
```

You could now reference a variable/param in your stylesheet like you would in a layout file:

```
{{ .Site.Params.container_size_wide | default "1100px" }}
```

Using within scss files requires a few extra steps. I wrote a more [indepth tutorial](https://blog.fullstackdigital.com/how-to-use-hugo-template-variables-in-scss-files-in-2018-b8a834accce) on how to use Hugo template variables in SCSS/CSS here.
