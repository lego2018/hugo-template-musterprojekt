+++
title       = "Sitemap for humans"
date        = 2017-07-14T15:40:34+01:00
description = "This description comes from sitemap.md"

bodyclass   = "sitemap-human"
+++

The sitemap is called with the shortcode `sitemap-human`.

{{< sitemapHuman >}}
